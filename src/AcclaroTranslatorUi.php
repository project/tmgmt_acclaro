<?php

namespace Drupal\tmgmt_acclaro;

use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\tmgmt\JobInterface;
use Drupal\tmgmt\SourcePreviewInterface;
use Drupal\tmgmt\TranslatorPluginUiBase;
use Drupal\tmgmt_acclaro\Plugin\tmgmt\Translator\AcclaroTranslator;

/**
 * Acclaro translator UI.
 */
class AcclaroTranslatorUi extends TranslatorPluginUiBase {
  use StringTranslationTrait;
  use DependencySerializationTrait;

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    /** @var \Drupal\tmgmt\TranslatorInterface $translator */
    $translator = $form_state->getFormObject()->getEntity();

    $form['token'] = [
      '#type' => 'textfield',
      '#title' => t('Web Token'),
      '#default_value' => $translator->getSetting('token'),
      '#description' => $this->t('Please enter your Web Token or visit <a href="@url">My Acclaro Portal</a> to get one.', ['@url' => 'https://my.acclaro.com/portal/apireference.php']),
    ];
    $form['use_sandbox'] = [
      '#type' => 'checkbox',
      '#title' => t('Use the sandbox'),
      '#default_value' => $translator->getSetting('use_sandbox'),
      '#description' => t('Check to use the testing environment.'),
    ];
    $form += parent::addConnectButton();

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::validateConfigurationForm($form, $form_state);

    /** @var \Drupal\tmgmt\TranslatorInterface $translator */
    $translator = $form_state->getFormObject()->getEntity();
    if ($translator->getSetting('token')) {
      $account = $translator->getPlugin()->getAccount($translator);
      if (empty($account)) {
        $form_state->setError($form['plugin_wrapper']['settings']['token'], t('Web token is not valid.'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function checkoutInfo(JobInterface $job) {
    $form = [];

    if ($job->isActive()) {
      $remote_mappings = $job->getRemoteMappings();
      /** @var \Drupal\tmgmt\Entity\RemoteMapping $remote_mapping */
      $remote_mapping = reset($remote_mappings);
      $translator = $job->getTranslator();

      $form['actions']['pull'] = [
        '#type' => 'submit',
        '#value' => $this->t('Fetch translations'),
        '#submit' => [[$this, 'submitFetchTranslations']],
        '#weight' => -10,
      ];

      $order_id = $remote_mapping->getRemoteIdentifier2();
      $acclaro_url = ($translator->getSetting('use_sandbox') ? AcclaroTranslator::SANDBOX_URL : AcclaroTranslator::PRODUCTION_URL) . '/orders/details/' . $order_id;
      $form['acclaro_project_link'] = [
        '#type' => 'item',
        '#title' => $this->t('Acclaro project link'),
        '#markup' => '<a href="' . $acclaro_url . '" target="_blank">' . $order_id . '</a>',
        '#weight' => -11,
      ];

      // Display simulate operations in case translator uses sandbox API key.
      if ($job->getTranslator()->getSetting('use_sandbox') && $remote_mapping) {
        $form['actions']['simulate_complete'] = [
          '#type' => 'submit',
          '#value' => $this->t('Simulate complete'),
          '#submit' => [[$this, 'submitSimulateOrderComplete']],
        ];

        // Check whether source plugin of the first item supports preview mode.
        $form['actions']['simulate_preview'] = [
          '#type' => 'submit',
          '#value' => $this->t('Simulate preview'),
          '#submit' => [[$this, 'submitSimulateTranslationPreview']],
          '#access' => $remote_mapping->getJobItem()->getSourcePlugin() instanceof SourcePreviewInterface,
        ];
      }

      $form['manual'] = [
        '#type' => 'details',
        '#title' => $this->t('Upload Files'),
        '#open' => FALSE,
      ];

      $form['manual']['file'] = [
        '#type' => 'file',
        '#title' => t('File'),
        '#size' => 50,
        '#multiple' => TRUE,
      ];
      $form['manual']['submit'] = [
        '#type' => 'submit',
        '#value' => t('Import'),
        '#submit' => [[$this, 'submitManualTranslationUpload']],
      ];
    }

    return $form;
  }

  /**
   * Submission callback for manual translation uploads.
   */
  public function submitManualTranslationUpload(array $form, FormStateInterface $form_state) {
    $job = $form_state->getFormObject()->getEntity();

    // Remove the destination query part, so we stay on the job.
    $request = \Drupal::request();
    if ($request->query->has('destination')) {
      $request->query->remove('destination');
    }

    // Ensure we have the file uploaded.
    $files = file_save_upload('file', [ 'file_validate_extensions' => ['xlf']]);
    if (empty(array_filter($files))) {
      $job->addMessage('There was an error while uploading the file. Check that you uploaded the correct file(s).');
      return;
    }

    foreach ($files as $file) {
      $extension = pathinfo($file->getFileUri(), PATHINFO_EXTENSION);
      $plugin = \Drupal::service('plugin.manager.tmgmt_file.format')->createInstance($extension);
      if ($plugin) {
        // Validate the file on job.
        $validated_job = $plugin->validateImport($file->getFileUri(), $job);
        if (!$validated_job) {
          $job->addMessage('Failed to validate file, import aborted.', [], 'error');
        }
        elseif ($validated_job->id() != $job->id()) {
          $job->addMessage('The imported file job id @file_id does not match the job id @job_id.', [
            '@file_id' => $validated_job->id(),
            '@job_id' => $job->id(),
          ], 'error');
        }

        else {
          try {
            // Validation successful, start import.
            $job->addTranslatedData($plugin->import($file->getFileUri()));
            $job->addMessage('Successfully imported file.');
          }
          catch (Exception $e) {
            $job->addMessage('File import failed with the following message: @message', ['@message' => $e->getMessage()], 'error');
          }
        }
      }
    }

    tmgmt_write_request_messages($job);
  }

  /**
   * Submit callback to fetch translations from Acclaro.
   */
  public function submitFetchTranslations(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\tmgmt\Entity\Job $job */
    $job = $form_state->getFormObject()->getEntity();

    /** @var \Drupal\tmgmt_acclaro\Plugin\tmgmt\Translator\AcclaroTranslator $translator_plugin */
    $translator_plugin = $job->getTranslator()->getPlugin();
    $translator_plugin->fetchTranslations($job);
  }

  /**
   * Submit callback to simulate completed orders from Acclaro.
   */
  public function submitSimulateOrderComplete(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\tmgmt\Entity\Job $job */
    $job = $form_state->getFormObject()->getEntity();

    /** @var \Drupal\tmgmt_acclaro\Plugin\tmgmt\Translator\AcclaroTranslator $translator_plugin */
    $translator_plugin = $job->getTranslator()->getPlugin();
    $translator_plugin->simulateCompleteOrder($job);
  }

  /**
   * Submit callback to simulate translation previews from Acclaro.
   */
  public function submitSimulateTranslationPreview(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\tmgmt\Entity\Job $job */
    $job = $form_state->getFormObject()->getEntity();
    /** @var \Drupal\tmgmt_acclaro\Plugin\tmgmt\Translator\AcclaroTranslator $translator_plugin */
    $translator_plugin = $job->getTranslator()->getPlugin();
    $translator_plugin->simulateTranslationPreview($job);
  }

  /**
   * {@inheritdoc}
   */
  public function checkoutSettingsForm(array $form, FormStateInterface $form_state, JobInterface $job) {
    $settings['name'] = [
      '#type' => 'textfield',
      '#title' => t('Name'),
      '#default_value' => $job->getSetting('name'),
      '#description' => t('Set the name for the Acclaro order. By default, a job label will be used.'),
    ];
    $settings['comment'] = [
      '#type' => 'textfield',
      '#title' => t('Comment'),
      '#default_value' => $job->getSetting('comment'),
      '#description' => t('Set the comment for the Acclaro order.'),
    ];
    if (!$job->isContinuous()) {
      $settings['duedate'] = [
        '#type' => 'date',
        '#title' => t('Due Date'),
        '#default_value' => $job->getSetting('duedate'),
        '#description' => t('The deadline for providing a translation.'),
      ];
    }

    return $settings;
  }

}
